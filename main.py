from scipy.spatial.distance import cosine
import numpy as np
f = open('TransH/entity2vec.txtbern')
g = open('TransH/relation2vec.txtbern')
lines_e = f.readlines()
lines_r = g.readlines()
line1 = lines_e[66196]
line2 = lines_r[0]

line1 = [float(i) for i in line1.split()]
line2 = [float(i) for i in line2.split()]
for i in range(1000):
    print 'Enter question:'
    q = str.lower(raw_input())
    vec = np.array([0.0 for _ in range(100)])
    entity_list = open('TransH/entities.txt').readlines()
    vecs = [np.asarray([float(i) for i in l.strip().split()]) for l in  open('TransH/entity2vec.txtbern').readlines()]
    entity_list = [str.lower(i.strip()) for i in entity_list]
    rel_vecs = [np.asarray([float(i) for i in l.strip().split()]) for l in  open('TransH/relation2vec.txtbern').readlines()]

    for i,e in enumerate(entity_list):
        if e in q and ((q.index(e) == 0 or q[q.index(e)-1] == ' ') and (q.index(e) + len(e) == len(q) or q[q.index(e) + len(e)] == ' ')):
            vec += vecs[entity_list.index(e)]
    if 'directed' in q:
        vec += rel_vecs[0]
    if 'written' in q or 'wrote' in q:
        vec += rel_vecs[1]
    if 'starred' in q or 'acted' in q or 'actors' in q:
        vec += rel_vecs[2]
    if 'released' in q:
        vec += rel_vecs[3]
    if 'language' in q:
        vec += rel_vecs[4]
    if 'tags' in q:
        vec += rel_vecs[5]
    if 'genre' in q:
        vec += rel_vecs[7]


    mn = 10
    cnt = 0
    ans = 0
    for line in lines_e:
        line = [float(l) for l in line.split()]
        dist = np.linalg.norm(vec - np.asarray(line))
        if dist < mn:
            mn = dist
            ans = cnt
        cnt += 1
    print entity_list[ans]
